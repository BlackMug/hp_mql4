//+------------------------------------------------------------------+
//|                                                    HP_Filter.mq4 |
//|                                                   Roman Soloview |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#include "curve.mqh"

#property copyright "Roman Soloview"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
// #property indicator_separate_window 
#property indicator_buffers 6
#property indicator_plots   6
//--- plot HP
#property indicator_label1  "Hodrick-Prescott original"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrAliceBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2
//--- plot Difference
#property indicator_label2  "History original"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot HP
#property indicator_label3  "Hodrick-Prescott straight"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrLemonChiffon
#property indicator_style3  STYLE_SOLID
#property indicator_width3  2
//--- plot Difference
#property indicator_label4  "History straight"
#property indicator_type4   DRAW_LINE
#property indicator_color4  clrYellow
#property indicator_style4  STYLE_SOLID
#property indicator_width4  1
//--- plot HP
#property indicator_label5  "Hodrick-Prescott tangent"
#property indicator_type5   DRAW_LINE
#property indicator_color5  clrAliceBlue
#property indicator_style5  STYLE_SOLID
#property indicator_width5  2
//--- plot Difference
#property indicator_label6  "History tangent"
#property indicator_type6   DRAW_LINE
#property indicator_color6  clrLightSkyBlue
#property indicator_style6  STYLE_SOLID
#property indicator_width6  1

#define                    EXTRA_OFFSET_COMMON 10;
#define                    NO_EXTRA_OFFSET 0

//--- input params
input int                  lambda = 1600;
input int                  extra_offset_straight_input = EXTRA_OFFSET_COMMON;
input int                  extra_offset_tangent_input = EXTRA_OFFSET_COMMON;
input int                  plot_deep_input = 500;
input double               tangent_scale_input = 1;
input string               out_file_name = "Difference";
input const int            average_deep = 10;

// Indicators buffer classes
HPCurve                    original("original", NO_EXTRA_OFFSET, plot_deep_input);
HPCurveStraight            straight("straight", extra_offset_straight_input, plot_deep_input);
HPCurveTangent             tangent("tangent", extra_offset_tangent_input, plot_deep_input);

datetime                   historyDateTime[];

bool                       isUnresized = false;

static double              maxValue = -99999;
static double              minValue = -maxValue;
static double              common_history_offset;


int OnInit()
{
   //--- indicator buffers mapping
   SetIndexBuffer(0,original.curve);
   SetIndexBuffer(1,original.history);
   SetIndexBuffer(2,straight.curve);
   SetIndexBuffer(3,straight.history);
   SetIndexBuffer(4,tangent.curve);
   SetIndexBuffer(5,tangent.history);

   tangent.tangentScale = tangent_scale_input;

   SetIndexShift( 2, straight.extraOffset ); // offset for straighted smoothing
   SetIndexShift( 4, tangent.extraOffset ); // offset for tangented smoothing

   common_history_offset = MathMax(straight.extraOffset, tangent.extraOffset);

   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
   writeHistoryToFile();
}


int OnCalculate(const int rates_total,
 const int prev_calculated,
 const datetime &time[],
 const double &open[],
 const double &high[],
 const double &low[],
 const double &close[],
 const long &tick_volume[],
 const long &volume[],
 const int &spread[])
{  
   int n = rates_total;

   if (n <= 3)
      return(rates_total);
   int plot_deep = MathMin( n, plot_deep_input );

   // If new bar
   // and we have enought bars for calculate history
   if (isNewBar(n))
   {
      original.calculateCurve(open);
      straight.calculateCurve(open);
      tangent.calculateCurve(open);

      original.storeHistoryValue(time);
      straight.storeHistoryValue(time);
      tangent.storeHistoryValue(time);

      minValue = MathMin( minValue, open[0] );
      maxValue = MathMax( maxValue, open[0] );
   }

   return(rates_total);
}

void writeHistoryToFile()
{
      maxValue = MathMax(maxValue, straight.maxValue);
      maxValue = MathMax(maxValue, tangent.maxValue);
      maxValue = MathMax(maxValue, original.maxValue);
      minValue = MathMin(maxValue, straight.minValue);
      minValue = MathMin(maxValue, tangent.minValue);
      minValue = MathMin(maxValue, original.minValue);

      PrintFormat(original.toString());
      PrintFormat(straight.toString());
      PrintFormat(tangent.toString());

      int n = MathMin( ArraySize(original.historyPoints), ArraySize(straight.historyPoints) );
      n = MathMin( n, ArraySize(tangent.historyPoints) );

      original.writeHistoryToFile(out_file_name, n, maxValue, minValue );
      straight.writeHistoryToFile(out_file_name, n, maxValue, minValue );
      tangent.writeHistoryToFile(out_file_name, n, maxValue, minValue );
}

double getNormalDiff(double curveVal, double curveHistory, double minVal, double maxVal)
{
   double hose = maxVal-minVal;
   double normalHPVal = (curveVal-minValue)/hose;
   double normalcurveHistoryVal = (curveHistory-minValue)/hose;
   return normalHPVal - normalcurveHistoryVal;
}

bool isNewBar(int n)
{
   static int curN = EMPTY;
   if (n > curN)
   {
      curN = n;
      return true;
   }
   return false;
}
