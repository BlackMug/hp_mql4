template<typename T>
void pushFront(T &array[], T value)
{
   int arraySize = ArraySize( array );
   ArraySetAsSeries(array, !ArrayGetAsSeries( array ));
   ArrayResize( array, arraySize+1 );
   ArraySetAsSeries(array, !ArrayGetAsSeries( array ));
   array[0] = value;
}

#define COMPARE_DEEP 100
#define VERY_BIG_DOUBLE 99999999999999


class HistoryPoint
{
public:
   HistoryPoint(double in_curve, double in_history, double in_average_history, datetime in_time, double in_orig_val)
   {
      curve = in_curve;
      history = in_history;
      averageHistory = in_average_history;
      time = in_time;
      orig_val = in_orig_val;
   }

   string toString()
   {
      return (
         StringFormat("curve = %lf | history = %lf | average = %lf | time = %s | orig_val = %lf"
            , curve, history, averageHistory, TimeToStr(time), orig_val)
      );
   }

public:   
   // This two get with offset 100 ticks
   double   curve;
   double   history;
   double   orig_val;

   double   averageHistory;
   datetime time;
};

// TODO: return plotDeep

class HPCurve
{
public:
   HPCurve(string initName, int initExtraOffset, int initPlotDeep) 
   {
      name = initName;
      extraOffset = initExtraOffset;
      plotDeep = initPlotDeep;
      maxValue = -VERY_BIG_DOUBLE; 
      minValue = -maxValue;
      maxDifference = -VERY_BIG_DOUBLE;
      minDifference = -maxDifference;
   }
   ~HPCurve()
   {
      for (int i = 0; i < ArraySize(historyPoints); i++)
         delete historyPoints[i];
   }

   const string toString() const 
   {
      return(
         StringFormat("!! %s: {curve size = %d} {history size = %d}  extraOffset=%d"
                        "  plotDeep=%d  minValue=%lf  maxValue=%lf "
                     , name, ArraySize(curve), ArraySize(history), extraOffset, plotDeep, minValue, maxValue)
      );
   }

   virtual void calculateCurve(const double &open[])
   {
      int n = MathMin(plotDeep, ArraySize(open));
      // int n = ArraySize(open);

      // TODO: Try to replace with ArrayCopy()
      for (int i = 0; i < n; i++)
            curve[i] = open[i];

      last_open = open[0];

      smoothWithHodrickPrescott(n);
   }
      
   void smoothWithHodrickPrescott(int n)
   {
      double a[]; ArrayResize(a,n);
      double b[]; ArrayResize(b,n);
      double c[]; ArrayResize(c,n);

      for (int i = n-1; i >= 0; i--)
      {
         a[i] = 6*lambda+1;
         b[i] = -4*lambda;
         c[i] = lambda;
      }

      a[0] = a[n-1] = 1+lambda;
      a[1] = a[n-2] = 5*lambda+1;
      b[0] = b[n-2] = -2*lambda;
      b[n-1] = c[n-2] = c[n-1] = 0;
      c[0] = lambda;

      double h1,h2,h3,h4,h5,hh2,hh3,hh5;
      h1=h2=h3=h4=h5=hh2=hh3=hh5 = 0;
      for(int i = 0; i < n; i++)
      {
         double z = a[i]-h4*h1-hh5*hh2;
         double hb = b[i];
         double hh1 = h1;
         h1 = (hb-h4*h2)/z;
         b[i] = h1;
         double hc = c[i];
         hh2 = h2;
         h2 = hc/z;
         c[i] = h2;
         a[i] = (curve[i] - hh3*hh5-h3*h4)/z;
         hh3 = h3;
         h3 = a[i];
         h4 = hb-h5*hh1;
         hh5 = h5;
         h5 = hc;
      }
      h2 = 0;
      h1 = a[n-1];
      curve[n-1] = h1;

      for (int i = n-1; i >= 0; i--)
      {
         curve[i] = a[i]-b[i]*h1-c[i]*h2;

         h2 = h1;
         h1 = curve[i];
      }
   }

   void storeHistoryValue( const datetime &curTimeArray[] )
   {
      int historySize = ArraySize( averageHistory );
      // Dirty fix for dirty bug
      if (historySize > 2)
      {
         if (history[1] == EMPTY_VALUE)
         {
            double val = (history[0]+history[2])/2;
            history[1] = val;
            // pushFront(time, curTimeArray[1]);
            PrintFormat("EMPTY_VALUE!! curTimeArray[1]: %s", TimeToStr(curTimeArray[1]));
         }
      }

      history[0] = curve[extraOffset];
      pushFront(orig_data, last_open);
      // pushFront(time, curTimeArray[0]);

      double averageDiff = 0;
      int n = MathMin( average_deep, historySize );
      if (n > 0)
      {
         for (int j = 0; j < n; j++)
         {
            averageDiff += MathAbs((curve[extraOffset+j] - history[j])); // TODO: abs
         }
         averageDiff = averageDiff / n;
      }
      pushFront(averageHistory, averageDiff);

      if (historySize > COMPARE_DEEP)
      {
         pushFront(historyPoints, new HistoryPoint(curve[COMPARE_DEEP+extraOffset], history[COMPARE_DEEP], averageHistory[COMPARE_DEEP], curTimeArray[COMPARE_DEEP], orig_data[COMPARE_DEEP]) );
      }

      maxValue = MathMax( maxValue, history[0] );
      minValue = MathMin( minValue, history[0] );
   }

   void writeHistoryToFile(string prefix, int n, double commonMaxValue, double commonMinValue)
   {
      int fileNumber = 0;
      string nameCommonPart = prefix+"_"+name+"_"+"offset"+IntegerToString(extraOffset) + "_";
      string fileName = nameCommonPart+IntegerToString(fileNumber)+".csv";
      while (FileIsExist(fileName))
      {
         fileNumber++;
         fileName = nameCommonPart+IntegerToString(fileNumber)+".csv";
      }


      int file_handle=FileOpen(fileName, FILE_WRITE|FILE_CSV);
      if(file_handle!=INVALID_HANDLE)
      {
         PrintFormat("File %s openned for writing",fileName);

         FileWrite(file_handle, "Max value", maxValue);
         FileWrite(file_handle, "Min value", minValue);

         double maxDiff = -99999;
         double minDiff = -maxDiff;
         double sumaryDiff = 0;

         int historySize = ArraySize(historyPoints);

         FileWrite(file_handle, this.toString()); 
         FileWrite(file_handle, "n = ", n);
         FileWrite(file_handle, "{historyPoints size} = ", historySize);

         for( int i = 0; i < historySize; i++ )
         {
            double curveVal = historyPoints[i].curve;
            double historyVal = historyPoints[i].history;
            double diff = MathAbs(curveVal - historyVal);
            double normalDiff = MathAbs(
               normalize(curveVal, commonMaxValue, commonMinValue) - 
               normalize(historyVal, commonMaxValue, commonMinValue));
            // double normalDiff = normalizeDiff(diff, commonMaxValue, commonMinValue);

            minDiff = MathMin( minDiff, diff );
            maxDiff = MathMax( maxDiff, diff );
            sumaryDiff += normalDiff;

            FileWrite(file_handle, i, historyPoints[i].time, normalDiff, historyPoints[i].curve, historyPoints[i].history, historyPoints[i].orig_val);
         }
         FileWrite(file_handle, "Max diff", maxDiff);
         FileWrite(file_handle, "Min diff", minDiff);
         FileWrite(file_handle, "Summary diff", sumaryDiff);
         
         FileClose(file_handle);
         PrintFormat("Data has been writen. File %s closed.",fileName);
      }
      else
         PrintFormat("Unable to open file %s, error code = %d",fileName,GetLastError());
   }

   double normalizeDiff(double val, double max, double min)
   {     
      return (val)/(max-min);
   }
   double normalize(double val, double max, double min)
   {
      return (val-min)/(max-min);
   }

public:
   string   name;
   int      extraOffset;
   int      plotDeep;
   double   minValue;
   double   maxValue;
   double   minDifference;
   double   maxDifference;

   double last_open;

   double   curve[];
   double   history[];
   double   orig_data[];
   double   averageHistory[];
   HistoryPoint* historyPoints[];
};



class HPCurveStraight: public HPCurve
{
public:
   HPCurveStraight(string initName, int initExtraOffset, int initPlotDeep)
      : HPCurve(initName, initExtraOffset, initPlotDeep) {}

   virtual void calculateCurve(const double &open[])
   {
      int n = MathMin(plotDeep, ArraySize(open));
      // int n = ArraySize(open);

      for( int i = 0; i < n; i++ )
      {
         if (i < extraOffset)
            curve[i] = open[0];
         else
            curve[i] = open[i-extraOffset];
      }

      last_open = open[0];

      smoothWithHodrickPrescott(n);
   }
};

class HPCurveTangent: public HPCurve
{
public:
   HPCurveTangent(string initName, int initExtraOffset, int initPlotDeep, double initTangentScale = 1)
      : HPCurve(initName, initExtraOffset, initPlotDeep) 
      {
         this.tangentScale = initTangentScale;
      }

   virtual void calculateCurve(const double &open[])
   {
      int n = MathMin(plotDeep, ArraySize(open));
      // int n = ArraySize(open);

      // Get tangent
      ArrayCopy( curve, open, 0, 0, WHOLE_ARRAY );
      smoothWithHodrickPrescott(n);
      double tan = curve[0]-curve[1];  // Simple tangent

      int i = extraOffset;
      curve[i] = open[0];
      while (i --> 0)
      {
         curve[i] = curve[i+1]+tan*tangentScale;
      }

      for( i = extraOffset; i < n; i++ )
      {
         curve[i] = open[i-extraOffset];
      }

      last_open = open[0];

      smoothWithHodrickPrescott(n);
   }

public:
   double tangentScale;
};